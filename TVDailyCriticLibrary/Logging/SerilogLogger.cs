﻿using Serilog;
using Serilog.Formatting.Json;
using System;

namespace TVDailyCriticLibrary.Logging
{
    public class SerilogLogger : ISerilogLogger
    {
        private readonly ISerializationManager serializationManager;

        public SerilogLogger()
        {
            serializationManager = new SerializationManager();

            Log.Logger = new LoggerConfiguration()
                        .MinimumLevel.Information()
                        .WriteTo.Console(new JsonFormatter())
                        .CreateLogger();
        }

        public void LogError<T>(T obj)
        {
            var data = serializationManager.Serialize(obj);
            try
            {
                Log.Logger.Error<T>("{@data}", obj);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void LogInfo<T>(T obj)
        {
            var data = serializationManager.Serialize(obj);
            try
            {
                Log.Logger.Information<T>("{@data}", obj);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void LogWarning<T>(T obj)
        {
            var data = serializationManager.Serialize(obj);
            try
            {
                Log.Logger.Warning<T>("{@data}", obj);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public void LogFatal<T>(T obj)
        {
            var data = serializationManager.Serialize(obj);
            try
            {
                Log.Logger.Fatal<T>("{@data}", obj);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
