﻿namespace TVDailyCriticLibrary.Logging
{
    public interface ISerilogLogger
    {
        void LogInfo<T>(T obj);
        void LogError<T>(T obj);
        void LogWarning<T>(T obj);
        void LogFatal<T>(T obj);
    }
}
