﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace TVDailyCriticLibrary.Logging
{
    public class SerializationManager : ISerializationManager
    {
        private readonly JsonSerializerSettings settings;

        public SerializationManager()
        {
            this.settings = new JsonSerializerSettings()
            {
                Formatting = Formatting.Indented,
                NullValueHandling = NullValueHandling.Ignore,
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore
            };

            this.settings.Converters.Add(new StringEnumConverter());
        }

        public T Deserialize<T>(string input)
        {
            return JsonConvert.DeserializeObject<T>(input, this.settings);
        }

        public string Serialize(object obj)
        {
            return JsonConvert.SerializeObject(obj, this.settings);
        }
    }
}
