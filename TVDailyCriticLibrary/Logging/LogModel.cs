﻿using System;
using System.Reflection;
using TVDailyCriticLibrary.Enumerations;

namespace TVDailyCriticLibrary.Logging
{
    public class LogModel
    {
        public DateTime CreatedDate => DateTime.UtcNow;
        public LogCategory LogCategory { get; set; } = LogCategory.Information;
        public string Application => Assembly.GetEntryAssembly().GetName().Name;
        public string Message { get; set; }
        public string StackTrace { get; set; }
        public string BaseException { get; set; }
        public string InnerException { get; set; }
    }
}
