﻿namespace TVDailyCriticLibrary.Logging
{
    public interface ISerializationManager
    {
        string Serialize(object input);
        T Deserialize<T>(string input);
    }
}
