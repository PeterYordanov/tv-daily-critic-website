﻿using System;
using System.Collections.Generic;

namespace TVDailyCriticLibrary.Extensions
{
    public static class ZipExtensions
    {
        public static IEnumerable<TResult> ZipSix<T1, T2, T3, T4, T5, T6, TResult>(
                this IEnumerable<T1> source,
                IEnumerable<T2> second,
                IEnumerable<T3> third,
                IEnumerable<T4> four,
                IEnumerable<T5> five,
                IEnumerable<T6> six,
                Func<T1, T2, T3, T4, T5, T6, TResult> func)
        {
            using var e1 = source.GetEnumerator();
            using var e2 = second.GetEnumerator();
            using var e3 = third.GetEnumerator();
            using var e4 = four.GetEnumerator();
            using var e5 = five.GetEnumerator();
            using var e6 = six.GetEnumerator();
            while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext() && e4.MoveNext() && e5.MoveNext() && e6.MoveNext())
                yield return func(e1.Current, e2.Current, e3.Current, e4.Current, e5.Current, e6.Current);
        }

        public static IEnumerable<TResult> ZipFive<T1, T2, T3, T4, T5, TResult>(
                this IEnumerable<T1> source,
                IEnumerable<T2> second,
                IEnumerable<T3> third,
                IEnumerable<T4> four,
                IEnumerable<T5> five,
                Func<T1, T2, T3, T4, T5, TResult> func)
        {
            using var e1 = source.GetEnumerator();
            using var e2 = second.GetEnumerator();
            using var e3 = third.GetEnumerator();
            using var e4 = four.GetEnumerator();
            using var e5 = five.GetEnumerator();
            while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext() && e4.MoveNext() && e5.MoveNext())
                yield return func(e1.Current, e2.Current, e3.Current, e4.Current, e5.Current);
        }

        public static IEnumerable<TResult> ZipFour<T1, T2, T3, T4, TResult>(
                this IEnumerable<T1> source,
                IEnumerable<T2> second,
                IEnumerable<T3> third,
                IEnumerable<T4> four,
                Func<T1, T2, T3, T4, TResult> func)
        {
            using var e1 = source.GetEnumerator();
            using var e2 = second.GetEnumerator();
            using var e3 = third.GetEnumerator();
            using var e4 = four.GetEnumerator();
            while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext() && e4.MoveNext())
                yield return func(e1.Current, e2.Current, e3.Current, e4.Current);
        }

        public static IEnumerable<TResult> ZipThree<T1, T2, T3, TResult>(
                this IEnumerable<T1> source,
                IEnumerable<T2> second,
                IEnumerable<T3> third,
                Func<T1, T2, T3, TResult> func)
        {
            using var e1 = source.GetEnumerator();
            using var e2 = second.GetEnumerator();
            using var e3 = third.GetEnumerator();
            while (e1.MoveNext() && e2.MoveNext() && e3.MoveNext())
                yield return func(e1.Current, e2.Current, e3.Current);
        }
    }
}
