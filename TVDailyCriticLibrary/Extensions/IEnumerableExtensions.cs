﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace TVDailyCriticLibrary.Extensions
{

	public static class IEnumerableExtensions
	{
		public static bool IsNullOrEmpty<T>(this IEnumerable<T> obj)
			where T : class
		{
			return obj == null || !obj.Any();
		}

		public static IEnumerable<T> DistinctBy<T, TKey>(this IEnumerable<T> items, Func<T, TKey> property)
		{
			return items.GroupBy(property).Select(x => x.First());
		}
	}
}
