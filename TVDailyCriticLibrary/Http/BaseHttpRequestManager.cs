﻿using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace TVDailyCriticLibrary.HTTP
{
    public abstract class BaseHttpRequestManager
    {
        protected IDictionary<string, string> KeyValuePairs = new Dictionary<string, string>();

        public void AddParameters(IDictionary<string, string> parameters)
        {
            KeyValuePairs = parameters;
        }

        public abstract Task<HttpResponseMessage> PostAsync(string uri, object obj, string mediaType);
        public abstract Task<HttpResponseMessage> PostAsync(string uri, string mediaType);
        public abstract Task<HttpResponseMessage> GetAsync(string uri);
        public abstract Task<HttpResponseMessage> PutAsync(string uri, object obj, string mediaType);
        public abstract Task<HttpResponseMessage> DeleteAsync(string uri, object obj, string mediaType);
        public abstract Task<Stream> DownloadToStreamAsync(string uri);
    }

}
