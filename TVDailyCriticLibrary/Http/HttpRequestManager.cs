﻿using Newtonsoft.Json;
using System;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace TVDailyCriticLibrary.HTTP
{
    public class HttpRequestManager : BaseHttpRequestManager
    {
        public override async Task<HttpResponseMessage> PostAsync(string uri, string mediaType)
        {
            HttpResponseMessage responseMessage = default;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Accept", mediaType);

                using (var request = new HttpRequestMessage(HttpMethod.Post, uri))
                {
                    request.Content = new FormUrlEncodedContent(KeyValuePairs);
                    responseMessage = await client.SendAsync(request);
                }
            }

            return responseMessage;
        }

        public async override Task<HttpResponseMessage> PostAsync(string url, object obj, string contentType)
        {
            string content = JsonConvert.SerializeObject(obj);
            var data = new StringContent(content, Encoding.UTF8, contentType);
            HttpResponseMessage responseMessage = default;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Accept", contentType);

                using (var request = new HttpRequestMessage(HttpMethod.Post, url))
                {
                    request.Content = data;
                    responseMessage = await client.SendAsync(request);
                }
            }

            return responseMessage;
        }

        public override async Task<HttpResponseMessage> DeleteAsync(string uri, object obj, string contentType)
        {
            string content = JsonConvert.SerializeObject(obj);
            var data = new StringContent(content, Encoding.UTF8, contentType);
            HttpResponseMessage responseMessage = default;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Accept", contentType);

                using (var request = new HttpRequestMessage(HttpMethod.Delete, uri))
                {
                    request.Content = data;
                    responseMessage = await client.SendAsync(request);
                }
            }

            return responseMessage;
        }

        public override async Task<HttpResponseMessage> PutAsync(string uri, object obj, string contentType)
        {
            string content = JsonConvert.SerializeObject(obj);
            var data = new StringContent(content, Encoding.UTF8, contentType);
            HttpResponseMessage responseMessage = default;

            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Accept", contentType);

                using (var request = new HttpRequestMessage(HttpMethod.Put, uri))
                {
                    request.Content = data;
                    responseMessage = await client.SendAsync(request);
                }
            }

            return responseMessage;
        }

        public override async Task<HttpResponseMessage> GetAsync(string uri)
        {
            HttpResponseMessage responseMessage = default;

            using (var client = new HttpClient())
            {
                responseMessage = await client.GetAsync(uri);
            }

            return responseMessage;
        }

        public override async Task<Stream> DownloadToStreamAsync(string uri)
        {
            Stream stream = new MemoryStream();
            using (HttpClient client = new HttpClient())
            {
                using (HttpRequestMessage request = new HttpRequestMessage(HttpMethod.Get, new Uri(uri)))
                {
                    using (Stream contentStream = await (await client.SendAsync(request)).Content.ReadAsStreamAsync())
                    {
                        await contentStream.CopyToAsync(stream);
                    }
                }
            }

            return stream;
        }
    }
}
