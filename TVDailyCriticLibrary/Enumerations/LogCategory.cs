﻿namespace TVDailyCriticLibrary.Enumerations
{
    public enum LogCategory
    {
        Information,
        Warning,
        Error,
        Fatal,
    }
}
