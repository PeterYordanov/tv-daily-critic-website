﻿using Microsoft.AspNetCore.Mvc;

namespace TVDailyCriticUI.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Contact()
        {
            return View();
        }

        public IActionResult FAQ()
        {
            return View();
        }

        public IActionResult TOS()
        {
            return View();
        }

        public IActionResult AccessDenied()
        {
            return View();
        }
    }
}
