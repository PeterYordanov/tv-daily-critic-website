﻿using Microsoft.AspNetCore.Mvc;
using TVDailyCriticUI.ViewModels;
using TVDailyCriticUI.Services;
using System.Collections.Generic;

namespace TVDailyCriticUI.Controllers
{
    public class UserController : Controller
    {
        private readonly IUserService _userService;
        private readonly ICommentService _commentService;

        public UserController(IUserService userService, ICommentService commentService)
        {
            _userService = userService;
            _commentService = commentService;
        }

        [HttpGet]
        public IActionResult Account()
        {
            var user = new UserViewModel();
            return View(user);
        }

        [HttpPost]
        public IActionResult SignUp(UserViewModel model)
        {
            _userService.SignUp(model);
            return RedirectToAction("Account", "User");
        }

        [HttpPost]
        public IActionResult SignIn(UserViewModel model)
        {
            UserViewModel user = _userService.SignIn(model);

            if (user != null)
            {
                return View("Profile", user);
            }
            else
            {
                ViewBag.Error = true;
                return RedirectToAction("Account", "User");
            }
        }

        [HttpGet]
        public IActionResult SignOut()
        {
            _userService.SignOut();
            return RedirectToAction("Index", "Home");
        }

        [HttpGet]
        public IActionResult Profile()
        {
            UserViewModel user = _userService.GetProfile();
            return View(user);
        }

        public IActionResult Profile(UserViewModel user)
        {
            if (!user.Password.Equals(user.ConfirmPassword, System.StringComparison.CurrentCulture))
            {
                return View();
            }
            _userService.UpdateProfile(user);
            return RedirectToAction("Account", "User");
        }

        public IActionResult Settings(UserViewModel user)
        {
            _userService.Settings(user);
            return RedirectToAction("Profile", "User");
        }

        public IActionResult List()
        {
            var users = _userService.List();
            return View(users);
        }

        public IActionResult Details(int id)
        {
            var user = _userService.Get(id);
            return View(user);
        }

        public IActionResult Create()
        {
            return View(new UserViewModel { });
        }

        public IActionResult Insert(UserViewModel user)
        {
            _userService.Create(user);
            return RedirectToAction("List", "User");
        }

        public IActionResult Update(int id)
        {
            UserViewModel viewModel = _userService.Get(id);
            return View(viewModel);
        }

        public IActionResult Edit(UserViewModel user)
        {
            _userService.Update(user);
            return RedirectToAction("List", "User");
        }

        public IActionResult Delete(int id)
        {
            List<CommentViewModel> comments = _commentService.List();
            bool result = false;
            foreach (var comment in comments)
            {
                if (comment.UserId.Equals(id))
                {
                    result = _commentService.IsDeleted(comment.CommentId);
                }
            }

            if (_userService.IsDeleted(id))
            {
                return RedirectToAction("List", "User");
            }

            return RedirectToAction("Account", "User");
        }

    }
}