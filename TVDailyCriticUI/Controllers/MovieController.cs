﻿using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using TVDailyCriticUI.AuthHandler;
using TVDailyCriticUI.Services;
using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.Controllers
{
    public class MovieController : Controller
    {
        private readonly IMovieService _movieService;
        private readonly IUserService _userService;
        private readonly SessionManager _sessionManager;

        public MovieController(IMovieService movieService, IUserService userService, SessionManager sessionManager)
        {
            _movieService = movieService;
            _userService = userService;
            _sessionManager = sessionManager;
        }

        public IActionResult List(FilterMovieViewModel filter)
        {
            var model = new FilterMovieViewModel();
            var movies = new List<MovieViewModel>();
            if (filter.Movie != null)
            {
                movies = _movieService.List().Where(x => x.MovieTitle.ToLower().Contains(filter.Movie.MovieTitle.ToLower())).ToList();
            }
            else
            {
                movies = _movieService.List();
            }
            model.Movies = movies;
            return View(model);
        }

        public IActionResult Create()
        {
            return View(new MovieViewModel
            {
            });
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public IActionResult Insert([Bind("ImageFile,CreatedDate,ReleaseDate,Description,MovieTitle,Producer,Writer,Director")] MovieViewModel movie)
        {
            if (ModelState.IsValid)
            {
                if (movie.ImageFile.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        movie.ImageFile.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        movie.Image = fileBytes;
                    }
                }
                movie.AddedBy = _sessionManager.UserId;
                _movieService.Create(movie);
            }
            return RedirectToAction("List", "Movie");
        }

        public IActionResult Details(int id)
        {
            var movie = _movieService.Get(id);
            return View(movie);
        }

        public IActionResult RatingsStatistics(int id)
        {
            MovieViewModel movieViewModel = _movieService.Get(id);
            List<UserViewModel> users = _userService.List();

            RatingsStatisticsViewModel viewModel = new RatingsStatisticsViewModel();
            List<CommentViewModel> commentsViewModel = new List<CommentViewModel>(movieViewModel.Comments);

            foreach(var item in commentsViewModel)
            {
                viewModel.MeanRating += item.Rating;
            }

            List<UserViewModel> tempUsers = new List<UserViewModel>(users);
            foreach(var user in tempUsers)
            {
                int index = commentsViewModel.FindIndex(f => f.UserId == user.UserId);
                if(!(index >= 0))
                {
                    users.Remove(user);
                }
            }

            viewModel.TotalUsers = users.Count;

            //Don't divide by zero
            if (commentsViewModel.Count != 0)
            {
                viewModel.MeanRating /= commentsViewModel.Count;
                viewModel.ModeRating = commentsViewModel.GroupBy(x => x.Rating).OrderByDescending(grp => grp.Count()).Select(grp => grp.Key).First();
                viewModel.PercentageMenRating = 100 * users.FindAll(x => x.Gender == 0).Count / commentsViewModel.Count;
                viewModel.PercentageWomenRating = 100 * users.FindAll(x => x.Gender == 1).Count / commentsViewModel.Count;
                viewModel.PercentageAdultsRating = 100 * users.FindAll(x => x.Age >= 18).Count / commentsViewModel.Count;
                viewModel.PercentageTeenagersRating = 100 * users.FindAll(x => x.Age < 18 && x.Age > 12).Count / commentsViewModel.Count;
            }

            viewModel.MovieTitle = movieViewModel.MovieTitle;

            return View(viewModel);
        }

        public IActionResult Delete(int id)
        {
            if (_movieService.IsDeleted(id))
            {
                return RedirectToAction("List", "Movie");
            }

            return RedirectToAction("Account", "User");
        }

        public IActionResult Search(FilterMovieViewModel filter)
        {
            var model = new FilterMovieViewModel();
            var movies = new List<MovieViewModel>();
            if (filter.Movie != null)
            {
                movies = _movieService.Search(filter);
            }
            else
            {
                movies = _movieService.List();
            }

            model.Movies = movies;
            return View(model);
        }

        public IActionResult Update(int id)
        {
            var movie = _movieService.Get(id);
            return View(movie);
        }

        [HttpPost]
        public IActionResult Edit([Bind("MovieId,ImageFile,CreatedDate,ReleaseDate,Description,MovieTitle")] MovieViewModel movie)
        {
            if (ModelState.IsValid)
            {
                if (movie.ImageFile.Length > 0)
                {
                    using (var ms = new MemoryStream())
                    {
                        movie.ImageFile.CopyTo(ms);
                        var fileBytes = ms.ToArray();
                        movie.Image = fileBytes;
                    }
                }
                movie.AddedBy = _sessionManager.UserId;
                _movieService.Update(movie);
            }
            return RedirectToAction("List", "Movie");
        }
    }
}