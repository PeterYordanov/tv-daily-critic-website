﻿using Microsoft.AspNetCore.Mvc;
using TVDailyCriticUI.ViewModels;
using TVDailyCriticUI.Services;
using TVDailyCriticUI.AuthHandler;

namespace TVDailyCriticUI.Controllers
{
    public class CommentController : Controller
    {
        private readonly ICommentService _commentService;
        private readonly SessionManager _sessionManager;

        public CommentController(ICommentService commentService, SessionManager sessionManager)
        {
            _commentService = commentService;
            _sessionManager = sessionManager;
        }

        public IActionResult Insert(CommentViewModel comment)
        {
            comment.UserId = _sessionManager.UserId;
            _commentService.Create(comment);
            return RedirectToAction("List", "Movie");
        }

        public IActionResult Create(int id)
        {
            var vm = new CommentViewModel
            {
                 UserId = _sessionManager.UserId,
                 MovieId = id
            };
            return View(vm);
        }

        public IActionResult Delete(int id)
        {
            if (_commentService.IsDeleted(id))
            {
                return RedirectToAction("List", "Movie");
            }
            return RedirectToAction("Account", "User");
        }
    }
}