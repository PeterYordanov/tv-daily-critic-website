﻿using Microsoft.AspNetCore.Http;
using System.Text;
using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.AuthHandler
{
    public class SessionManager
    {
        private readonly IHttpContextAccessor _httpContextAccessor;

        public SessionManager(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public string CurrentRole
        {
            get
            {
                string role = string.Empty;
                if (_httpContextAccessor.HttpContext.Session.Get("Role") != null)
                {
                    role = Encoding.UTF8.GetString(_httpContextAccessor.HttpContext.Session.Get("Role"));
                }
                return role;
            }
        }

        public string Email
        {
            get
            {
                string role = string.Empty;
                if (_httpContextAccessor.HttpContext.Session.Get("Email") != null)
                {
                    role = Encoding.UTF8.GetString(_httpContextAccessor.HttpContext.Session.Get("Email"));
                }
                return role;
            }
        }

        public int UserId { get; set; }
        public bool IsAdmin { get; set; }
        public bool IsStateLogged { get; set; }

        public void SetUserSession(UserViewModel user)
        {
            var httpContext = _httpContextAccessor.HttpContext;
            httpContext.Session.SetString("UserId", user.UserId.ToString());
            httpContext.Session.SetString("Email", user.Email);
            UserId = user.UserId;
            if (user.Role.Contains("Admin"))
            {
                IsAdmin = true;
            }
            else
            {
                IsAdmin = false;
            }
            httpContext.Session.SetString("Role", user.Role);
            httpContext.Session.SetString("IsStateLogged", "true");
            IsStateLogged = true;
        }

        public void ClearSession()
        {
            var httpContext = _httpContextAccessor.HttpContext;
            httpContext.Session.SetString("Email", string.Empty);
            httpContext.Session.SetString("IsStateLogged", "false");
            httpContext.Session.SetString("Role", "None");
            IsStateLogged = false;
        }
    }
}
