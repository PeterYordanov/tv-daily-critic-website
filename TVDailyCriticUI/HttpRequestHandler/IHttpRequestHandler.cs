﻿using System.Collections.Generic;
using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.HttpRequestHandler
{
    public interface IHttpRequestHandler<T> where T : class
    {
        List<T> List(string str);
        List<T> Search(string str, FilterMovieViewModel model);
        T Get(string str, int id);
        T Create(string str, T model);
        T Update(string str, T model);
        UserViewModel GetProfile(string str, int id);
        UserViewModel SignIn(UserViewModel user);
        string SignUp(T user);
        bool Delete(string str, int id);
    }
}
