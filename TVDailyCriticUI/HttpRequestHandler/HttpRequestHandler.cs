﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.HttpRequestHandler
{
    public class HttpRequestHandler<T> : IHttpRequestHandler<T> where T : class
    {
        private readonly string HeaderValue = "application/json";
        private readonly string HeaderName = "Accept";
        private readonly string Url = "https://localhost:44397/";

        public List<T> Search(string str, FilterMovieViewModel model)
        {
            var result = new List<T>();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(HeaderName, HeaderValue);
                var serialized = JsonConvert.SerializeObject(model.Movie);
                var deserialized = JsonConvert.DeserializeObject<Dictionary<string, string>>(serialized);
                var query = deserialized.Select((p) => p.Key.ToString() + "=" + Uri.EscapeDataString(p.Value)).Aggregate((p1, p2) => p1 + "&" + p2);
                string response = client.GetStringAsync(Url + str + "?" + query).Result;
                result = JsonConvert.DeserializeObject<List<T>>(response);
            }
            return result;
        }

        public List<T> List(string str)
        {
            var result = new List<T>();
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(HeaderName, HeaderValue);
                var response = client.GetStringAsync(Url + str).Result;
                result = JsonConvert.DeserializeObject<List<T>>(response);
            }
            return result;
        }

        public T Get(string str, int id)
        {
            T result = null;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(HeaderName, HeaderValue);
                var response = client.GetAsync(Url + str + "/" + id).Result;
                var body = client.GetStringAsync(Url + str + "/" + id).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = JsonConvert.DeserializeObject<T>(body);
                }
            }
            return result;
        }

        public UserViewModel GetProfile(string str, int id)
        {
            UserViewModel result;
            using (var client = new HttpClient())
            {
                client.DefaultRequestHeaders.Add(HeaderName, HeaderValue);
                var response = client.GetStringAsync(Url + str + "/" + id).Result;
                result = JsonConvert.DeserializeObject<UserViewModel>(response);
            }
            return result;
        }

        public T Create(string str, T model)
        {
            using (var client = new HttpClient())
            {
                var content = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue(HeaderValue);
                var response = client.PostAsync(new Uri(Url + str), byteContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    var body = response.Content.ReadAsStringAsync().Result;
                    model = JsonConvert.DeserializeObject<T>(body);
                }
            }
            return model;
        }

        public T Update(string str, T model)
        {
            using (var client = new HttpClient())
            {
                var content = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue(HeaderValue);
                var response = client.PutAsync(new Uri(Url + str), byteContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    var body = response.Content.ReadAsStringAsync().Result;
                    model = JsonConvert.DeserializeObject<T>(body);
                }
            }
            return model;
        }

        public UserViewModel SignIn(UserViewModel model)
        {
            using (var client = new HttpClient())
            {
                var content = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue(HeaderValue);
                var response = client.PostAsync(new Uri(Url + "users/signin"), byteContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    var body = response.Content.ReadAsStringAsync().Result;
                    model = JsonConvert.DeserializeObject<UserViewModel>(body);
                }
            }
            return model;
        }

        public string SignUp(T model)
        {
            var result = string.Empty;
            using (var client = new HttpClient())
            {
                var content = JsonConvert.SerializeObject(model);
                var buffer = Encoding.UTF8.GetBytes(content);
                var byteContent = new ByteArrayContent(buffer);
                byteContent.Headers.ContentType = new MediaTypeHeaderValue(HeaderValue);
                var response = client.PostAsync(new Uri(Url + "users/signup"), byteContent).Result;
                if (response.IsSuccessStatusCode)
                {
                    result = response.Content.ReadAsStringAsync().Result;
                }
            }
            return result;
        }

        public bool Delete(string str, int id)
        {
            var result = true;
            using (var client = new HttpClient())
            {
                //client.BaseAddress = new Uri(Url + str);
                client.DefaultRequestHeaders.Add(HeaderName, HeaderValue);
                var response = client.DeleteAsync(Url + str + "/" + id).Result;
                if (!response.IsSuccessStatusCode)
                {
                    result = false;
                }
            }
            return result;
        }
    }
}
