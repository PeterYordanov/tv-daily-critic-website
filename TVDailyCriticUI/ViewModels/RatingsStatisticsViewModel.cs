﻿namespace TVDailyCriticUI.ViewModels
{
    public class RatingsStatisticsViewModel
    {
        public int PercentageMenRating { get; set; }
        public int PercentageWomenRating { get; set; }
        public int PercentageAdultsRating { get; set; }
        public int PercentageTeenagersRating { get; set; }
        public int ModeRating { get; set; }
        public int MeanRating { get; set; }
        public string MovieTitle { get; set; }
        public int TotalUsers { get; set; }
    }
}
