﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TVDailyCriticUI.ViewModels
{
    public class MovieViewModel
    {
        public int MovieId { get; set; }

        [Display(Name = "Created date")]
        [DataType(DataType.Date)]
        public DateTime CreatedDate { get; set; }

        [Display(Name = "Release Date")]
        [DataType(DataType.Date)]
        public DateTime ReleaseDate { get; set; }

        [Display(Name = "Title")]
        public string MovieTitle { get; set; }

        public string Description { get; set; }
        public string Director { get; set; }
        public string Producer { get; set; }
        public string Writer { get; set; }

        public byte[] Image { get; set; }

        [NotMapped]
        public virtual ICollection<UserViewModel> Users { get; set; }
        [NotMapped]
        public virtual ICollection<CommentViewModel> Comments { get; set; }

        [NotMapped]
        public IFormFile ImageFile { get; set; }
        public int AddedBy { get; set; }
    }
}