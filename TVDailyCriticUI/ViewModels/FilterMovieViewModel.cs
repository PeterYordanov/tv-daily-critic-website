﻿using System.Collections.Generic;
using TVDailyCriticDAL.CustomModel;

namespace TVDailyCriticUI.ViewModels
{
    public class FilterMovieViewModel
    {
        public virtual ICollection<MovieViewModel> Movies { get; set; }
        public virtual FilterMovie Movie { get; set; }
    }
}
