﻿using System.ComponentModel.DataAnnotations;
using TVDailyCriticDAL.Models;

namespace TVDailyCriticUI.ViewModels
{
    public class UserViewModel
    {
        public int UserId { get; set; }
        [Display(Name = "First name")]
        public string Firstname { get; set; }

        [Display(Name = "Last name")]
        public string Lastname { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Confirm Password")]
        public string ConfirmPassword { get; set; }

        [Display(Name = "Telephone")]
        public string Telephone { get; set; }

        [Display(Name = "Gender")]
        public int Gender { get; set; }

        [Display(Name = "Age")]
        public int Age { get; set; }

        [Display(Name = "Role")]
        public string Role { get; set; }
    }
}
