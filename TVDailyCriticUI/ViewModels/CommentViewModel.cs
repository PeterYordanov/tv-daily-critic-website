﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TVDailyCriticUI.ViewModels
{
    public class CommentViewModel
    {
        public int CommentId { get; set; }
        [Display(Name = "Comment")]
        public string CommentText { get; set; }
        public int MovieId { get; set; }
        [ForeignKey("MovieId")]
        public virtual MovieViewModel Movie { get; set; }
        public int UserId { get; set; }
        [ForeignKey("UserId")]
        public virtual UserViewModel User { get; set; }
        public virtual ICollection<UserViewModel> Users { get; set; }
        public virtual ICollection<MovieViewModel> Movies { get; set; }
        public int Rating { get; set; }
    }
}
