﻿using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.Services
{
    public interface IUserService : IService<UserViewModel>
    {
        string SignUp(UserViewModel model);
        void UpdateProfile(UserViewModel user);
        void Settings(UserViewModel user);
        void SignOut();
        UserViewModel SignIn(UserViewModel model);
        UserViewModel GetProfile();
    }
}
