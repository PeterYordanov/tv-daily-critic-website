﻿using System.Collections.Generic;
using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.Services
{
    public interface IMovieService : IService<MovieViewModel>
    {
        MovieViewModel Update(MovieViewModel model);
        MovieViewModel Create(MovieViewModel model);
        List<MovieViewModel> Search(FilterMovieViewModel model);
    }
}