﻿using System;
using System.Collections.Generic;
using TVDailyCriticUI.AuthHandler;
using TVDailyCriticUI.HttpRequestHandler;
using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.Services
{
    public class CommentService : ICommentService
    {
        private readonly IHttpRequestHandler<CommentViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public CommentService(IHttpRequestHandler<CommentViewModel> httpService, SessionManager sessionManager) 
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public bool CanCreate()
        {
            return _sessionManager.IsStateLogged;
        }

        public CommentViewModel Create(CommentViewModel model)
        {
            CommentViewModel comment = new CommentViewModel();
            if (_sessionManager.IsStateLogged == true)
            {
                comment = _httpService.Create("api/comments", model);
            }
            return comment;
        }

        public CommentViewModel Get(int id)
        {
            return default;
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin)
            {
                result = _httpService.Delete("api/comments", id);
            }
            return result;
        }

        public List<CommentViewModel> List()
        {
            return _httpService.List("api/comments");
        }

        public CommentViewModel Update(CommentViewModel model)
        {
            return default;
        }
    }
}
