﻿using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.Services
{
    public interface ICommentService : IService<CommentViewModel>
    {
    }
}
