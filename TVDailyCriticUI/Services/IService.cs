﻿using System.Collections.Generic;

namespace TVDailyCriticUI.Services
{
    public interface IService<T>
    {
        List<T> List();
        T Update(T model);
        T Create(T model);
        T Get(int id);
        
        bool CanCreate();
        bool IsDeleted(int id);
    }
}
