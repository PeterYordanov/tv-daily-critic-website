﻿using System.Collections.Generic;
using TVDailyCriticUI.AuthHandler;
using TVDailyCriticUI.HttpRequestHandler;
using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.Services
{
    public class MovieService : GenericService, IMovieService
    {
        private readonly IHttpRequestHandler<MovieViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public MovieService(IHttpRequestHandler<MovieViewModel> httpService, SessionManager sessionManager) 
            : base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public MovieViewModel Create(MovieViewModel model)
        {
            return _httpService.Create("api/movies", model);
        }

        public MovieViewModel Get(int id)
        {
            return _httpService.Get("api/movies", id) ?? new MovieViewModel();
        }

        public bool IsDeleted(int id)
        {
            var result = false;
            if (_sessionManager.IsAdmin)
            {
                result = _httpService.Delete("api/movies", id);
            }
            return result;

        }

        public List<MovieViewModel> List()
        {
            return _httpService.List("api/movies") ?? new List<MovieViewModel>();
        }

        public List<MovieViewModel> Search(FilterMovieViewModel model)
        {
            return _httpService.Search("movies/search", model) ?? new List<MovieViewModel>();
        }

        public MovieViewModel Update(MovieViewModel model)
        {
            return _httpService.Update("api/movies", model);
        }
    }
}
