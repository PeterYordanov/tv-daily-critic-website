﻿using System;
using System.Collections.Generic;
using TVDailyCriticUI.AuthHandler;
using TVDailyCriticUI.HttpRequestHandler;
using TVDailyCriticUI.ViewModels;

namespace TVDailyCriticUI.Services
{
    public class UserService : GenericService,
                               IService<UserViewModel>,
                               IUserService
    {
        private readonly IHttpRequestHandler<UserViewModel> _httpService;
        private readonly SessionManager _sessionManager;

        public UserService(IHttpRequestHandler<UserViewModel> httpService, SessionManager sessionManager) :
            base(sessionManager)
        {
            _httpService = httpService;
            _sessionManager = sessionManager;
        }

        public void Settings(UserViewModel user)
        {
            _httpService.Update("api/users", user);
        }

        public UserViewModel SignIn(UserViewModel model)
        {
            UserViewModel user = _httpService.SignIn(model);

            if (user != null)
            {
                _sessionManager.SetUserSession(user);
            }

            return user;
        }

        public void SignOut()
        {
            if (_sessionManager.IsStateLogged)
            {
                _sessionManager.ClearSession();
                _sessionManager.IsStateLogged = false;
            }
        }

        public string SignUp(UserViewModel model)
        {
            model.Role = "User";
            return _httpService.SignUp(model);
        }

        public UserViewModel Update(UserViewModel model)
        {
            var user = _httpService.Update("api/users", model);

            return user;
        }

        public void UpdateProfile(UserViewModel user)
        {
            _httpService.Update("api/users", user);
        }

        public List<UserViewModel> List()
        {
            return _httpService.List("api/users") ?? new List<UserViewModel>();
        }

        public UserViewModel Create(UserViewModel model)
        {
            UserViewModel user = new UserViewModel();
            if (_sessionManager.IsAdmin)
            {
                user = _httpService.Create("api/users", model);
            }
            return user;
        }

        public UserViewModel Get(int id)
        {
            return _httpService.Get("api/users", id) ?? new UserViewModel();
        }

        public UserViewModel GetProfile()
        {
            return _httpService.GetProfile("api/users", _sessionManager.UserId);
        }

        public bool IsDeleted(int id)
        {
            return _sessionManager.IsAdmin && _httpService.Delete("api/users", id) ? true : false;
        }
    }
}
