﻿using TVDailyCriticUI.AuthHandler;

namespace TVDailyCriticUI.Services
{
    public class GenericService
    {
        private SessionManager _sessionManager;

        public GenericService(SessionManager sessionManager)
        {
            _sessionManager = sessionManager;
        }

        public bool CanCreate()
        {
            return _sessionManager.IsAdmin;
        }
    }
}
