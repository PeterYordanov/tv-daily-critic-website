﻿using Microsoft.AspNetCore.Mvc;

namespace TVDailyCriticDAL.CustomModel
{
    public class FilterMovie
    {
        [FromQuery(Name = "MovieTitle")]
        public string MovieTitle { get; set; }
        [FromQuery(Name = "Description")]
        public string Description { get; set; }
    }
}

