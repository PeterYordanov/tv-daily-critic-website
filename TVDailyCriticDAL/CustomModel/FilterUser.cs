﻿using Microsoft.AspNetCore.Mvc;

namespace TVDailyCriticDAL.CustomModel
{
    public class FilterUser
    {
        [FromQuery(Name = "User")]
        public string Username { get; set; }
    }
}
