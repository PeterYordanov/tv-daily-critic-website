﻿using Microsoft.EntityFrameworkCore;
using TVDailyCriticDAL.Models;

namespace TVDailyCriticDAL.DatabaseContext
{
    public class TVDailyCriticContext : DbContext
    {
        public TVDailyCriticContext(DbContextOptions<TVDailyCriticContext> options)
            : base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<Comment> Comment { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}
