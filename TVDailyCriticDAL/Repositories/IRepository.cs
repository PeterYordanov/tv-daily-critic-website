﻿using TVDailyCriticDAL.Models;
using System.Collections.Generic;

namespace TVDailyCriticDAL.Repositories
{
    public interface IRepository<T> where T : class
    {
        List<T> GetAll();
        T GetById(int id);
        void Insert(T entity);
        void Edit(T obj);
        void Delete(T obj);
    }
}
