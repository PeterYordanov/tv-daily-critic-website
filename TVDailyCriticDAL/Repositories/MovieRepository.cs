﻿using System.Collections.Generic;
using System.Linq;
using TVDailyCriticDAL.Models;
using Microsoft.EntityFrameworkCore;
using TVDailyCriticDAL.DatabaseContext;

namespace TVDailyCriticDAL.Repositories
{
    public class MovieRepository<T> : IMovieRepository<T> where T : class
    {
        protected readonly TVDailyCriticContext _context;
        protected readonly DbSet<T> _table;

        public MovieRepository(TVDailyCriticContext context)
        {
            this._context = context;
            this._table = _context.Set<T>();
        }

        public List<Movie> GetMovies()
        {
            var query = from movie in _context.Movie
                        select new Movie
                        {
                            MovieId = movie.MovieId,
                            MovieTitle = movie.MovieTitle,
                            CreatedDate = movie.CreatedDate,
                            Description = movie.Description,
                            ReleaseDate = movie.ReleaseDate,
                            Director = movie.Director,
                            Producer = movie.Producer, 
                            Writer = movie.Writer,
                            Image = movie.Image
                        };

            var movies = query.ToList();
            return movies;
        }

        public Movie GetMovieById(int id)
        {
            var commentsQuery = from comment in _context.Comment
                                where comment.MovieId == id
                                select new Comment
                                {
                                    CommentId = comment.CommentId,
                                    CommentText = comment.CommentText,
                                    MovieId = id,
                                    Rating = comment.Rating,
                                    UserId = comment.UserId,
                                    User = comment.User
                                };

            var query = from movie in _context.Movie
                        where movie.MovieId == id
                        select new Movie
                        {
                            MovieId = movie.MovieId,
                            MovieTitle = movie.MovieTitle,
                            CreatedDate = movie.CreatedDate,
                            Description = movie.Description,
                            ReleaseDate = movie.ReleaseDate,
                            Director = movie.Director,
                            Producer = movie.Producer,
                            Writer = movie.Writer,
                            Image = movie.Image,
                            Comments = commentsQuery.ToList()
                        };

            var result = query.ToList();
            return result.FirstOrDefault();
        }

        public List<Movie> SearchMovies(string title, string description)
        {
            var query = from movie in _context.Movie
                        select new Movie
                        {
                            MovieId = movie.MovieId,
                            MovieTitle = movie.MovieTitle,
                            CreatedDate = movie.CreatedDate,
                            ReleaseDate = movie.ReleaseDate,
                            Description = movie.Description,
                        };
            var movies = query.ToList();

            var filtered = new List<Movie>();

            if (title != null)
            {
                filtered = movies.Where(p => p.MovieTitle.Contains(title)).ToList();
            }
            if (description != null)
            {
                filtered = movies.Where(p => p.Description == description).ToList();
            }

            return filtered;
        }

        public void Delete(Movie movie)
        {
            var comments = _context.Comment.Where(p => p.MovieId == movie.MovieId).ToList();
            if (comments != null)
            {
                foreach (var comment in comments)
                {
                    _context.Comment.Remove(comment);
                    _context.SaveChanges();
                }
            }
            _context.Movie.Remove(movie);
            _context.SaveChanges();
        }
    }
}
