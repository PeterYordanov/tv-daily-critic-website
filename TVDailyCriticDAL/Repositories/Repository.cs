﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using TVDailyCriticDAL.DatabaseContext;

namespace TVDailyCriticDAL.Repositories
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected readonly TVDailyCriticContext _context;
        protected readonly DbSet<T> _table;

        public Repository(TVDailyCriticContext context)
        {
            this._context = context;
            this._table = _context.Set<T>();
        }

        public List<T> GetAll()
        {
            return _table.ToList();
        }
  
        public T GetById(int id)
        {
            return _table.Find(id);
        }

        public void Insert(T entity)
        {
            _table.Add(entity);
            _context.SaveChanges();
        }

        public void Edit(T entity)
        {
            _table.Update(entity);
            _context.SaveChanges();
        }

        public void Delete(T obj)
        {
            _table.Remove(obj);
            _context.SaveChanges();
        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    _context.Dispose();
                }

                disposedValue = true;
            }
        }
        public void Dispose()
        {
            Dispose(true);
        }

        #endregion
    }
}
