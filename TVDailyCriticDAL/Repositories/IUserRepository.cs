﻿using TVDailyCriticDAL.Models;

namespace TVDailyCriticDAL.Repositories
{
	public interface IUserRepository
	{
		User Login(string email, string pass);
		User CheckDublicate(string email);
	}
}
