﻿using TVDailyCriticDAL.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using TVDailyCriticDAL.DatabaseContext;

namespace TVDailyCriticDAL.Repositories
{
	public class UserRepository : IUserRepository
	{
		protected readonly TVDailyCriticContext _context;
		protected readonly DbSet<User> _table;

		public UserRepository(TVDailyCriticContext context)
		{
			this._context = context;
			this._table = _context.Set<User>();
		}

		public User Login(string email, string pass)
		{
			return _context.User.ToList().Where(p => p.Email == email && p.Password == pass).FirstOrDefault();
		}

		public User CheckDublicate(string email)
		{
			return _context.User.ToList().Where(p => p.Email == email).FirstOrDefault();
		}
	}
}
