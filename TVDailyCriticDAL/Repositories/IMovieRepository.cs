﻿using TVDailyCriticDAL.Models;
using System.Collections.Generic;

namespace TVDailyCriticDAL.Repositories
{
    public interface IMovieRepository<T> where T : class
    {
        List<Movie> SearchMovies(string title, string description);
        List<Movie> GetMovies();
        Movie GetMovieById(int it);
        void Delete(Movie movie);
    }
}
