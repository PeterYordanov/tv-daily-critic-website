﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TVDailyCriticDAL.Models
{
    public class Movie
    {
        [Key]
        public int MovieId { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime ReleaseDate { get; set; }
        public string MovieTitle { get; set; }
        public string Description { get; set; }
        public string Director { get; set; }
        public string Producer { get; set; }
        public string Writer { get; set; }
        [NotMapped]
        public virtual IEnumerable<Comment> Comments { get; set; }
        public byte[] Image { get; set; }
    }
}
