﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TVDailyCriticDAL.Models
{
    public class Comment
    {
        [Key]
        public int CommentId { get; set; }
        public string CommentText  { get; set; }
        [ForeignKey("MovieId")]
        public int MovieId { get; set; }
        public virtual Movie Movie { get; set; }
        [ForeignKey("UserId")]
        public int UserId { get; set; }
        public virtual User User { get; set; }
        public int Rating { get; set; }
    }
}
