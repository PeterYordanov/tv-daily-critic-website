﻿using System.ComponentModel.DataAnnotations;

namespace TVDailyCriticDAL.Models
{
    public class User
    {
        [Key]
        public int UserId { get; set; }
        public string Firstname { get; set; }
        public string Lastname { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string Telephone { get; set; }
        public int Gender { get; set; }
        public int Age { get; set; }
        public string Role { get; set; }
    }
}
