﻿namespace TVDailyCriticAPI.Options
{
    public class MongoDbSettings
    {
        public string DatabaseName { get; set; }
        public string ConnectionString { get; set; }
        public string UserCollectionName { get; set; }
        public string MovieCollectionName { get; set; }
        public string CommentCollectionName { get; set; }
    }
}
