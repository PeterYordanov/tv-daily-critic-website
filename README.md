# TV Daily Critic Website

[[_TOC_]]

## CI/CD
[![pipeline status](https://gitlab.com/PeterYordanov/tv-daily-critic-website/badges/master/pipeline.svg)](https://gitlab.com/PeterYordanov/tv-daily-critic-website/commits/master)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)
  
<p>
<details>
<summary><h2>Screenshots</h2></summary>

![GrafanaDashboard](screenshots/GrafanaDashboard.PNG "GrafanaDashboard")
![PrometheusGrafana](screenshots/PrometheusGrafana.PNG "PrometheusGrafana")
![Prometheus](screenshots/Prometheus.PNG "Prometheus")
![Register](screenshots/Register.PNG "Register")
![MovieDetailsAdmin](screenshots/MovieDetailsAdmin.PNG "MovieDetailsAdmin")
![MoviesListUser](screenshots/MoviesListUser.PNG "MoviesListUser")
![Statistics](screenshots/Statistics.PNG "Statistics")
![UserDetails](screenshots/UserDetails.PNG "UserDetails")

</details>
</p>


## Tech Stack
- Bootstrap
- ASP.NET Core
- Swagger
- Entity Framework
- AppMetrics
- Prometheus (2.26.0)
- Grafana (7.5.4)

## System Design
```mermaid
graph TB
  subgraph "TV Daily Critic"
  UI("TV Daily Critic UI")
  UI -- invokes --> API("TV Daily Critic API")
  API -- uses --> DAL("TV Daily Critic DAL")
  end

  subgraph "Library"
  LIB("TV Daily Critic Library")
  LIB -- referenced by --> UI
  LIB -- referenced by --> API
  LIB -- referenced by --> DAL
  end

  subgraph "MSSQL"
  DB("Database")
  API -- queries --> DB
end
```

## Features
- [x] CI/CD
- [x] pre-commit hook
- [x] Docker support
- [x] Swagger interactive documentation
- [x] Prometheus
- [x] Grafana
- [x] AppMetrics
- [x] Operations
  - [x] Users
	- [x] Login/Register
	- [x] Profile
	- [x] Change details
	- [x] User Management (Admin)
		- [x] Add
		- [x] Edit
		- [x] Remove
		- [x] Get
		- [x] List
  - [x] Comments
	- [x] Add
	- [x] Remove (Admin)
	- [x] Get
	- [x] List
  - [x] Movies
    - [x] Add
	- [x] Edit (Admin)
	- [x] Remove (Admin)
	- [x] Get
	- [x] List
	- [x] Search
