﻿using TVDailyCriticAPI.Extensions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using TVDailyCriticDAL.DatabaseContext;
using TVDailyCriticDAL.Repositories;
using TVDailyCriticDAL.Models;
using Microsoft.AspNetCore.Http;

namespace TVDailyCriticAPI.Controllers
{
    [Route("api/[controller]")]
    public class CommentsController : ControllerBase
    {
        private readonly IRepository<Comment> _repo;

        public CommentsController(TVDailyCriticContext context)
        {
            _repo = new Repository<Comment>(context);
        }

        /// <summary>
        /// Return all comments
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public IActionResult Get()
        {
            var comments = _repo.GetAll();
            if (Condition.ValidateObjects(comments))
            {
                return NotFound();
            }
            return Ok(comments);
        }

        /// <summary>
        /// Return a comment from an ID
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var comment = _repo.GetById(id);
            if (Condition.ValidateObject(comment))
            {
                return NotFound();
            }
            return Ok(comment);
        }

        /// <summary>
        /// Insert comment
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public IActionResult Post([FromBody] Comment comment)
        {
            _repo.Insert(comment);
            if (Condition.ValidateObject(comment))
            {
                return NotFound();
            }
            return Created(Message.CreatedComment, comment);
        }

        /// <summary>
        /// Update comment
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut]
        public IActionResult Put([FromBody] Comment comment)
        {
            _repo.Edit(comment);
            return Ok(comment);
        }

        /// <summary>
        /// Delete comment by ID
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var comment = _repo.GetById(id);
            if (Condition.ValidateObject(comment))
            {
                return NotFound();
            }
            else
            {
                _repo.Delete(comment);
                return Ok();
            }
        }
    }
}
