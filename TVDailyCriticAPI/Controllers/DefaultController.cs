﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace TVDailyCriticAPI.Controllers
{
    [Route("api/[controller]")]
    public class DefaultController : ControllerBase
    {
        /// <summary>
        /// Index, for health checks
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        [Route("/")]
        [Route("/index")]
        [Route("/default")]
        public IActionResult Get()
        {
            return Ok("Ok");
        }
    }
}
