﻿using TVDailyCriticAPI.Extensions;
using Microsoft.AspNetCore.Mvc;
using TVDailyCriticDAL.Repositories;
using TVDailyCriticDAL.Models;
using TVDailyCriticDAL.DatabaseContext;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;

namespace TVDailyCriticAPI.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class UsersController : ControllerBase
    {
        private readonly IRepository<User> _repo;
        private readonly IUserRepository _userRepo;

        public UsersController(TVDailyCriticContext context)
        {
            _userRepo = new UserRepository(context);
            _repo = new Repository<User>(context);
        }

        /// <summary>
        /// Sign up
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        [Route("/[controller]/signup")]
        public IActionResult SignUp([FromBody] User user)
        {
			user.Firstname = user.Firstname.FirstLetterToUpperCase();
			user.Lastname = user.Lastname.FirstLetterToUpperCase();
			user.Email = user.Email.ToLower();
			user.Role = user.Role.FirstLetterToUpperCase();
			user.Password = StringExtension.SHA512(user.Password);
            _repo.Insert(user);

            if (Condition.ValidateUser(user, user.Firstname, user.Lastname, user.Email, user.Password, user.Role))
            {
                return BadRequest(Message.BadRequestUser);
            }
            return Created(Message.CreatedUser, user);
        }

        /// <summary>
        /// Sign in
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        [Route("/[controller]/signin")]
        public IActionResult SignIn([FromBody] User user)
        {
            var hashedPassword = StringExtension.SHA512(user.Password);
            var entity = _userRepo.Login(user.Email.ToLower(), hashedPassword);

            if (entity == null)
            {
                return Unauthorized(Message.Unauthorized);
            }
            _repo.Edit(entity);
            return Ok(entity);

        }

        /// <summary>
        /// Update user
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut]
        public IActionResult Put([FromBody] User user)
        {
            if (Condition.ValidateUser(user, user.Firstname, user.Lastname, user.Email, user.Password, user.Role))
            {
                return BadRequest(Message.BadRequestUser);
            }
            user.Password = StringExtension.SHA512(user.Password);
            _repo.Edit(user);
            return Ok(user);
        }

        /// <summary>
        /// Delete user by ID
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var user = _repo.GetById(id);
            if (Condition.ValidateObject(user))
            {
                return NotFound();
            }
            else
            {
                _repo.Delete(user);
                return Ok();
            }
        }

        /// <summary>
        /// Return user by ID
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        [AllowAnonymous]
        public IActionResult Get(int id)
        {
            var user = _repo.GetById(id);
            if (Condition.ValidateObject(user))
            {
                return NotFound();
            }
            return Ok(user);
        }

        /// <summary>
        /// Insert user
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public IActionResult Post([FromBody] User user)
        {
            user.Firstname = user.Firstname.FirstLetterToUpperCase();
            user.Lastname = user.Lastname.FirstLetterToUpperCase();
            user.Email = user.Email.ToLower();
            user.Role = user.Role.FirstLetterToUpperCase();
            user.Password = StringExtension.SHA512(user.Password);
            _repo.Insert(user);
            if (Condition.ValidateUser(user, user.Firstname, user.Lastname, user.Email, user.Password, user.Role))
            {
                return BadRequest(Message.BadRequestUser);
            }
            return Created(Message.CreatedUser, user);
        }

        /// <summary>
        /// Return all users
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public IActionResult Get()
        {
            var users = _repo.GetAll();
            if (Condition.ValidateObjects(users))
            {
                return NotFound();
            }
            return Ok(users);
        }
    }
}
