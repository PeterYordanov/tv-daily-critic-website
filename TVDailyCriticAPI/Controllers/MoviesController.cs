﻿using TVDailyCriticAPI.Extensions;
using Microsoft.AspNetCore.Mvc;
using System;
using TVDailyCriticDAL.DatabaseContext;
using TVDailyCriticDAL.Models;
using TVDailyCriticDAL.Repositories;
using TVDailyCriticDAL.CustomModel;
using Microsoft.AspNetCore.Http;

namespace TVDailyCriticAPI.Controllers
{
    [Route("api/[controller]")]
    public class MoviesController : ControllerBase
    {
        private readonly IMovieRepository<Movie> _movieRepo;
        private readonly IRepository<Movie> _repo;

        public MoviesController(TVDailyCriticContext context)
        {
            _movieRepo = new MovieRepository<Movie>(context);
            _repo = new Repository<Movie>(context);
        }

        /// <summary>
        /// Return all movies
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet]
        public IActionResult Get()
        {
            var movies = _movieRepo.GetMovies();
            if (Condition.ValidateObjects(movies))
            {
                return NotFound();
            }
            return Ok(movies);
        }

        /// <summary>
        /// Return movie by ID
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpGet("{id}")]
        public IActionResult Get(int id)
        {
            var movie = _movieRepo.GetMovieById(id);
            if (Condition.ValidateObject(movie))
            {
                return NotFound();
            }
            return Ok(movie);
        }

        /// <summary>
        /// Insert movie
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPost]
        public IActionResult Post([FromBody] Movie movie)
        {
            movie.CreatedDate = DateTime.Now;
            _repo.Insert(movie);
            if (Condition.ValidateObject(movie))
            {
                return NotFound();
            }
            return Created(Message.CreatedMovie, movie);
        }

        /// <summary>
        /// Update movie
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpPut]
        public IActionResult Put([FromBody] Movie movie)
        {
            _repo.Edit(movie);
            return Ok(movie);
        }

        /// <summary>
        /// Delete movie by ID
        /// </summary>
        [ProducesResponseType(typeof(IActionResult), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            var movie = _repo.GetById(id);
            if (Condition.ValidateObject(movie))
            {
                return NotFound();
            }
            else
            {
                _movieRepo.Delete(movie);
                return Ok();
            }
        }
    }
}
