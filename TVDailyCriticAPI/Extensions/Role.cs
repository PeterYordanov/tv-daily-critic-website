﻿namespace TVDailyCriticAPI.Extensions
{
    public static class Role
    {
        public const string Admin = "Admin";
        public const string BackOffice = "Back Office";
        public const string User = "User";
        public const string AdminBackOffice = "Admin, Back Office";
        public const string All = "Admin, Back Office, User";
    }
}
