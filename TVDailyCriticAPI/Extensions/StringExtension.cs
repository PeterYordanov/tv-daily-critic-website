﻿using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace TVDailyCriticAPI.Extensions
{
    public static class StringExtension
    {
        public static string FirstLetterToUpperCase(this string title)
        {
            return CultureInfo.CurrentCulture.TextInfo.ToTitleCase(title.ToLower());
        }

        private static string GenerateHashString(HashAlgorithm algo, string text)
        {
            algo.ComputeHash(Encoding.UTF8.GetBytes(text));

            var result = algo.Hash;

            return string.Join(
                string.Empty,
                result.Select(x => x.ToString("x2")));
        }

        public static string SHA512(string text)
        {
            var result = default(string);

            using (var algo = new SHA512Managed())
            {
                result = GenerateHashString(algo, text);
            }

            return result;
        }
    }
}